# About

Chef recipes to install go-docker master and slaves

http://gettingstartedwithchef.com/first-steps-with-chef.html

Cookbook contains recipes for master and slaves. On master, all elements (databases, processes) are installed on a single server. Configuration is using Docker Swarm executor. By default, it makes use of the local authentification plugin, creating a local *godocker* user (password godocker).
Master exports the shared directory to the slaves.

Configuration file is located at: /etc/godocker/go-d.ini

# Install chef and cookbooks

Warning: according to system, one may need to use sudo in commands

yum update -y | apt-get update

Install git and curl packages

    curl -L https://www.opscode.com/chef/install.sh | bash -s -- -v 12.6.0
    cd /opt
    git clone https://osallou@bitbucket.org/osallou/go-docker-chef.git

    cd go-docker-chef

Edit chef-repo/.chef/knife.rb and solo.rb files and update cookbook_path to the location where is installed the cloned repository if you did not put the code under /opt.

    cd chef-repo

# Master

    chef-solo -c solo.rb -j godocker-master.json

Web interface will be available on port 6543

# Slaves

After getting master ip address and updating godocker-slave.json with this ip.

    chef-solo -c solo.rb -j godocker-slave.json
