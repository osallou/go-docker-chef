#
# Cookbook Name:: godocker
# Recipe:: master
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

docker_image 'swarm' do
  action :pull
  host 'tcp://'+node['ipaddress']+':2375'
end

docker_image 'microbox/etcd' do
  action :pull
  host 'tcp://'+node['ipaddress']+':2375'
end

docker_image 'jplock/zookeeper' do
  action :pull
  host 'tcp://'+node['ipaddress']+':2375'
end

docker_image 'redis' do
  action :pull
  host 'tcp://'+node['ipaddress']+':2375'
end

docker_image 'mongo' do
  action :pull
  host 'tcp://'+node['ipaddress']+':2375'
end


#docker_image 'osallou/go-docker' do
#  action :pull
#end

package 'godocker_packages' do
  case node[:platform]
  when 'redhat', 'centos'
    package_name ['python', 'python-setuptools', 'nfs-utils', 'nfs-utils-lib', 'git']
  when 'ubuntu', 'debian'
    package_name ['python', 'python-setuptools', 'nfs-kernel-server', 'git']
  end
end


nfs_export "/opt/godshared" do
  network '*'
  writeable true
end


easy_install_package 'godocker_cli'

directory '/opt/godshared' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  recursive true
end

directory '/etc/godocker' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  recursive true
end

directory '/opt/godshared/home/godocker' do
  owner node['godocker']['user']['id']
  group node['godocker']['user']['gid']
  mode '0755'
  action :create
  recursive true
end


file "/var/log/godocker.log" do
  content "Local IP: "+node["ipaddress"]+"\n"
  owner "root"
  group "root"
  mode 00644
end


docker_container 'god-etcd' do
    repo 'microbox/etcd'
    name 'god-etcd'
    tag 'latest'
    host 'tcp://'+node['ipaddress']+':2375'
    port '4001:4001'
    command '-name godocker'
    binds ['/var/etcd/:/data']
    action :run
    remove_volumes true
end

docker_container 'god-zoo' do
    repo 'jplock/zookeeper'
    name 'god-zoo'
    tag 'latest'
    host 'tcp://'+node['ipaddress']+':2375'
    port ['2181:2181', '2888:2888', '3888:3888']
    action :run
    remove_volumes true
end

docker_container 'god-mongo' do
  repo 'mongo'
  name 'god-mongo'
  host 'tcp://'+node['ipaddress']+':2375'
  action :run
end

docker_container 'god-redis' do
  repo 'redis'
  name 'god-redis'
  host 'tcp://'+node['ipaddress']+':2375'
  action :run
end

execute 'wait_containers_ready' do
  command 'sleep 10'
end

docker_container 'god-swarm' do
    repo 'swarm'
    name 'god-swarm'
    tag 'latest'
    host 'tcp://'+node['ipaddress']+':2375'
    port "2376:2375"
    links ['god-zoo:god-zoo']
    command 'manage  zk://'+node['ipaddress']+'/godocker'
    action :run
    remove_volumes true
end

template '/etc/godocker/go-d.ini' do
  source 'go-d.ini.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables({
      :masterip => node['ipaddress']
      })
end

template '/etc/godocker/production.ini' do
  source 'production.ini.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

docker_image 'osallou/go-docker' do
  action :pull
  host 'tcp://'+node['ipaddress']+':2375'
end

docker_container 'god-create-user' do
  repo 'osallou/go-docker'
  name 'god-create-user'
  host 'tcp://'+node['ipaddress']+':2375'
  binds  ['/opt/godshared:/opt/godshared','/etc/godocker/go-d.ini:/opt/go-docker/go-d.ini']
  links ['god-mongo:god-mongo', 'god-redis:god-redis', 'god-swarm:god-swarm']
  command '/usr/bin/python seed/create_local_user.py --config /opt/go-docker/go-d.ini --login '+node['godocker']['user']['name']+' --password '+node['godocker']['user']['password']+' --uid '+node['godocker']['user']['uid'].to_s+' --gid '+node['godocker']['user']['gid'].to_s
  action :run
  remove_volumes true
end



docker_container 'god-web-init' do
  repo 'osallou/go-docker'
  name 'god-create-init'
  host 'tcp://'+node['ipaddress']+':2375'
  binds  ['/opt/godshared:/opt/godshared','/etc/godocker/go-d.ini:/opt/go-docker/go-d.ini']
  links ['god-mongo:god-mongo', 'god-redis:god-redis', 'god-swarm:god-swarm']
  command '/usr/bin/python go-d-scheduler.py init'
  action :run
  remove_volumes true
end

docker_container 'god-web' do
  repo 'osallou/go-docker'
  name 'god-web'
  host 'tcp://'+node['ipaddress']+':2375'
  binds  ['/opt/godshared:/opt/godshared','/etc/godocker/go-d.ini:/opt/go-docker/go-d.ini', '/etc/godocker/production.ini:/opt/go-docker-web/production.ini']
  links ['god-mongo:god-mongo', 'god-redis:god-redis', 'god-swarm:god-swarm']
  command 'gunicorn -p godweb.pid --log-config=/opt/go-docker-web/production.ini --paste /opt/go-docker-web/production.ini'
  port '6543:6543'
  env ['PYRAMID_ENV=prod']
  action :run
  remove_volumes true
end

docker_container 'god-scheduler' do
  repo 'osallou/go-docker'
  name 'god-scheduler'
  host 'tcp://'+node['ipaddress']+':2375'
  binds  ['/opt/godshared:/opt/godshared','/etc/godocker/go-d.ini:/opt/go-docker/go-d.ini']
  links ['god-web:god-web', 'god-mongo:god-mongo', 'god-redis:god-redis', 'god-swarm:god-swarm']
  command '/usr/bin/python go-d-scheduler.py run'
  action :run
  remove_volumes true
end

docker_container 'god-watcher' do
  repo 'osallou/go-docker'
  name 'god-watcher'
  host 'tcp://'+node['ipaddress']+':2375'
  binds  ['/opt/godshared:/opt/godshared','/etc/godocker/go-d.ini:/opt/go-docker/go-d.ini']
  links ['god-web:god-web', 'god-mongo:god-mongo', 'god-redis:god-redis', 'god-swarm:god-swarm']
  command '/usr/bin/python go-d-watcher.py run'
  action :run
  remove_volumes true
end
